package visitor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.boolQuery;

import org.elasticsearch.index.query.AbstractQueryBuilder;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchNoneQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.jupiter.api.Test;

public class QueryParserTest {

    QueryParser parser = new QueryParser();

    @Test
    public void testParseKeyValuePairSingleTerm() {
        AbstractQueryBuilder builder = parser.parse("(KEY:value)");
        assertThat(builder).isEqualTo(
                new MatchQueryBuilder("KEY", "value")
        );
    }

    @Test
    public void testParseKeyValuePairMultipleTerms() {
        AbstractQueryBuilder builder = parser.parse("(KEY:value1,value2)");

        assertThat(builder).isEqualTo(
                new MatchQueryBuilder("KEY", "value1 value2")
        );
    }

    @Test
    public void testParseMultipleKeyValuePairs() {
        AbstractQueryBuilder builder = parser.parse("(KEY1:value)(KEY2:value1,value2)");

        assertThat(builder).isEqualTo(
                boolQuery()
                        .should(new MatchQueryBuilder("KEY1", "value"))
                        .should(new MatchQueryBuilder("KEY2", "value1 value2"))
        );
    }

    @Test
    public void testParseInvalidKeyValuePairKey() {
        AbstractQueryBuilder builder = parser.parse("(9KEY:value)");

        assertThat(builder).isEqualTo(
                new MatchNoneQueryBuilder()
        );
    }

    @Test
    public void testParseInvalidKeyValuePairTerm() {
        AbstractQueryBuilder builder = parser.parse("(KEY:9value)");

        assertThat(builder).isEqualTo(
                new MatchNoneQueryBuilder()
        );
    }

    @Test
    public void testParseLogicANDQuery() {
        AbstractQueryBuilder builder = parser.parse("(KEY1:value1) AND (KEY2:value1,value2)");

        assertThat(builder).isEqualTo(
            boolQuery()
                .must(new MatchQueryBuilder("KEY1", "value1"))
                .must(new MatchQueryBuilder("KEY2", "value1 value2"))
        );
    }

    @Test
    public void testParseLogicORQuery() {
        AbstractQueryBuilder builder = parser.parse("(KEY1:value1) OR (KEY2:value1,value2)");

        assertThat(builder).isEqualTo(
            boolQuery()
                .should(new MatchQueryBuilder("KEY1", "value1"))
                .should(new MatchQueryBuilder("KEY2", "value1 value2"))
        );
    }

    @Test
    public void testParseNestedQueries() {
        AbstractQueryBuilder builder = parser.parse("(KEY1:value1) OR ((KEY2:value2) AND (KEY3:value3))");

        MatchQueryBuilder A = QueryBuilders.matchQuery("KEY1", "value1");
        MatchQueryBuilder B = QueryBuilders.matchQuery("KEY2", "value2");
        MatchQueryBuilder C = QueryBuilders.matchQuery("KEY3", "value3");

        BoolQueryBuilder bAndC = boolQuery().must(B).must(C);
        BoolQueryBuilder result = boolQuery().should(A).should(bAndC);

        assertThat(builder).isEqualTo(result);
    }

    @Test
    public void testParseNestedQueries2() {
        AbstractQueryBuilder builder = parser.parse("(KEY1:value1) AND ((KEY2:value2) OR (KEY3:value3))");

        MatchQueryBuilder A = QueryBuilders.matchQuery("KEY1", "value1");
        MatchQueryBuilder B = QueryBuilders.matchQuery("KEY2", "value2");
        MatchQueryBuilder C = QueryBuilders.matchQuery("KEY3", "value3");

        BoolQueryBuilder bOrC = boolQuery().should(B).should(C);
        BoolQueryBuilder result = boolQuery().must(A).must(bOrC);

        assertThat(builder).isEqualTo(result);
    }

    @Test
    public void testParseDiacriticsSymbolsUsage() {
        AbstractQueryBuilder builder = parser.parse("(KEY:väƛȗé)");

        assertThat(builder).isEqualTo(new MatchQueryBuilder("KEY", "väƛȗé"));
    }
}
