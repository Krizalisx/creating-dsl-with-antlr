// Generated from C:/Users/qpalz/Desktop/Repositories/Tutorials/creating-dsl-with-antlr/extra-assignments/src/main/antlr4/nl/luminis/meetup/visitor\KeyValuePairGrammar.g4 by ANTLR 4.8
package me.antlr.gen;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link KeyValuePairGrammarParser}.
 */
public interface KeyValuePairGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code parentheses}
	 * labeled alternative in {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 */
	void enterParentheses(KeyValuePairGrammarParser.ParenthesesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parentheses}
	 * labeled alternative in {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 */
	void exitParentheses(KeyValuePairGrammarParser.ParenthesesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Or}
	 * labeled alternative in {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 */
	void enterOr(KeyValuePairGrammarParser.OrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Or}
	 * labeled alternative in {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 */
	void exitOr(KeyValuePairGrammarParser.OrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code And}
	 * labeled alternative in {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 */
	void enterAnd(KeyValuePairGrammarParser.AndContext ctx);
	/**
	 * Exit a parse tree produced by the {@code And}
	 * labeled alternative in {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 */
	void exitAnd(KeyValuePairGrammarParser.AndContext ctx);
	/**
	 * Enter a parse tree produced by the {@code queryPair}
	 * labeled alternative in {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 */
	void enterQueryPair(KeyValuePairGrammarParser.QueryPairContext ctx);
	/**
	 * Exit a parse tree produced by the {@code queryPair}
	 * labeled alternative in {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 */
	void exitQueryPair(KeyValuePairGrammarParser.QueryPairContext ctx);
	/**
	 * Enter a parse tree produced by {@link KeyValuePairGrammarParser#pair}.
	 * @param ctx the parse tree
	 */
	void enterPair(KeyValuePairGrammarParser.PairContext ctx);
	/**
	 * Exit a parse tree produced by {@link KeyValuePairGrammarParser#pair}.
	 * @param ctx the parse tree
	 */
	void exitPair(KeyValuePairGrammarParser.PairContext ctx);
	/**
	 * Enter a parse tree produced by {@link KeyValuePairGrammarParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(KeyValuePairGrammarParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link KeyValuePairGrammarParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(KeyValuePairGrammarParser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link KeyValuePairGrammarParser#key}.
	 * @param ctx the parse tree
	 */
	void enterKey(KeyValuePairGrammarParser.KeyContext ctx);
	/**
	 * Exit a parse tree produced by {@link KeyValuePairGrammarParser#key}.
	 * @param ctx the parse tree
	 */
	void exitKey(KeyValuePairGrammarParser.KeyContext ctx);
}