// Generated from C:/Users/qpalz/Desktop/Repositories/Tutorials/creating-dsl-with-antlr/extra-assignments/src/main/antlr4/nl/luminis/meetup/visitor\KeyValuePairGrammar.g4 by ANTLR 4.8
package me.antlr.gen;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link KeyValuePairGrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface KeyValuePairGrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code parentheses}
	 * labeled alternative in {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParentheses(KeyValuePairGrammarParser.ParenthesesContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Or}
	 * labeled alternative in {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOr(KeyValuePairGrammarParser.OrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code And}
	 * labeled alternative in {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd(KeyValuePairGrammarParser.AndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code queryPair}
	 * labeled alternative in {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQueryPair(KeyValuePairGrammarParser.QueryPairContext ctx);
	/**
	 * Visit a parse tree produced by {@link KeyValuePairGrammarParser#pair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPair(KeyValuePairGrammarParser.PairContext ctx);
	/**
	 * Visit a parse tree produced by {@link KeyValuePairGrammarParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue(KeyValuePairGrammarParser.ValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link KeyValuePairGrammarParser#key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKey(KeyValuePairGrammarParser.KeyContext ctx);
}