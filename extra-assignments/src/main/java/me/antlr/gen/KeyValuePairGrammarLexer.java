// Generated from C:/Users/qpalz/Desktop/Repositories/Tutorials/creating-dsl-with-antlr/extra-assignments/src/main/antlr4/nl/luminis/meetup/visitor\KeyValuePairGrammar.g4 by ANTLR 4.8
package me.antlr.gen;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class KeyValuePairGrammarLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		AND=1, OR=2, U_CASE_WORD=3, L_CASE_WORD=4, COMBINATOR=5, SEPARATOR=6, 
		LEFTBRACKET=7, RIGHTBRACKET=8, WHITESPACE=9;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"ExtendedChar", "ExtendedStartChar", "AND", "OR", "U_CASE_WORD", "L_CASE_WORD", 
			"COMBINATOR", "SEPARATOR", "LEFTBRACKET", "RIGHTBRACKET", "WHITESPACE"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'AND'", "'OR'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "AND", "OR", "U_CASE_WORD", "L_CASE_WORD", "COMBINATOR", "SEPARATOR", 
			"LEFTBRACKET", "RIGHTBRACKET", "WHITESPACE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public KeyValuePairGrammarLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "KeyValuePairGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\13C\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\3\2\3\2\5\2\34\n\2\3\3\3\3\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\6"+
		"\3\6\7\6)\n\6\f\6\16\6,\13\6\3\7\3\7\7\7\60\n\7\f\7\16\7\63\13\7\3\b\3"+
		"\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\6\f>\n\f\r\f\16\f?\3\f\3\f\2\2\r\3\2"+
		"\5\2\7\3\t\4\13\5\r\6\17\7\21\b\23\t\25\n\27\13\3\2\n\4\2\62;C\\\4\2c"+
		"|\u0082\u0251\3\2C\\\3\2..\3\2<<\3\2**\3\2++\5\2\13\f\17\17\"\"\2D\2\7"+
		"\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2"+
		"\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\3\33\3\2\2\2\5\35\3\2\2\2\7"+
		"\37\3\2\2\2\t#\3\2\2\2\13&\3\2\2\2\r-\3\2\2\2\17\64\3\2\2\2\21\66\3\2"+
		"\2\2\238\3\2\2\2\25:\3\2\2\2\27=\3\2\2\2\31\34\5\5\3\2\32\34\t\2\2\2\33"+
		"\31\3\2\2\2\33\32\3\2\2\2\34\4\3\2\2\2\35\36\t\3\2\2\36\6\3\2\2\2\37 "+
		"\7C\2\2 !\7P\2\2!\"\7F\2\2\"\b\3\2\2\2#$\7Q\2\2$%\7T\2\2%\n\3\2\2\2&*"+
		"\t\4\2\2\')\t\2\2\2(\'\3\2\2\2),\3\2\2\2*(\3\2\2\2*+\3\2\2\2+\f\3\2\2"+
		"\2,*\3\2\2\2-\61\5\5\3\2.\60\5\3\2\2/.\3\2\2\2\60\63\3\2\2\2\61/\3\2\2"+
		"\2\61\62\3\2\2\2\62\16\3\2\2\2\63\61\3\2\2\2\64\65\t\5\2\2\65\20\3\2\2"+
		"\2\66\67\t\6\2\2\67\22\3\2\2\289\t\7\2\29\24\3\2\2\2:;\t\b\2\2;\26\3\2"+
		"\2\2<>\t\t\2\2=<\3\2\2\2>?\3\2\2\2?=\3\2\2\2?@\3\2\2\2@A\3\2\2\2AB\b\f"+
		"\2\2B\30\3\2\2\2\7\2\33*\61?\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}