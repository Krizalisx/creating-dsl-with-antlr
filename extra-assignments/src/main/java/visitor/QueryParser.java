package visitor;

import java.util.ArrayList;
import java.util.List;
import me.antlr.gen.KeyValuePairGrammarBaseVisitor;
import me.antlr.gen.KeyValuePairGrammarLexer;
import me.antlr.gen.KeyValuePairGrammarParser;
import me.antlr.gen.KeyValuePairGrammarParser.AndContext;
import me.antlr.gen.KeyValuePairGrammarParser.OrContext;
import me.antlr.gen.KeyValuePairGrammarParser.ParenthesesContext;
import me.antlr.gen.KeyValuePairGrammarParser.QueryPairContext;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;
import org.elasticsearch.index.query.AbstractQueryBuilder;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchNoneQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

public class QueryParser {

    private KeyValuePairGrammarVisitor visitor = new KeyValuePairGrammarVisitor();

    public AbstractQueryBuilder parse(String input) {
        KeyValuePairGrammarLexer lexer = new KeyValuePairGrammarLexer(CharStreams.fromString(input));
        lexer.addErrorListener(ParserErrorListener.getInstance());
        CommonTokenStream commonTokenStream = new CommonTokenStream(lexer);
        KeyValuePairGrammarParser parser = new KeyValuePairGrammarParser(commonTokenStream);
        parser.addErrorListener(ParserErrorListener.getInstance());

        try {
            KeyValuePairGrammarParser.QueryContext query = parser.query();
            return visitor.visit(query);
        } catch (ParseCancellationException e) {
            return new MatchNoneQueryBuilder();
        }
    }

    private class KeyValuePairGrammarVisitor extends KeyValuePairGrammarBaseVisitor<AbstractQueryBuilder> {

        @Override
        public AbstractQueryBuilder visitAnd(AndContext ctx) {
            AbstractQueryBuilder lhs = visit(ctx.query(0));
            AbstractQueryBuilder rhs = visit(ctx.query(1));

            return QueryBuilders.boolQuery().must(lhs).must(rhs);
        }

        @Override
        public AbstractQueryBuilder visitOr(OrContext ctx) {
            AbstractQueryBuilder lhs = visit(ctx.query(0));
            AbstractQueryBuilder rhs = visit(ctx.query(1));

            return QueryBuilders.boolQuery().should(lhs).should(rhs);
        }

        @Override
        public AbstractQueryBuilder visitQueryPair(QueryPairContext ctx) {
            return visit(ctx.pair());
        }

        @Override
        public AbstractQueryBuilder visitParentheses(ParenthesesContext ctx) {
            return visit(ctx.query());
        }

//        @Override
//        public AbstractQueryBuilder visitQuery(KeyValuePairGrammarParser.QueryContext ctx) {
//            BoolQueryBuilder res = QueryBuilders.boolQuery();
//            List<AbstractQueryBuilder> builders = new ArrayList<>();
//
//            ctx.pair().forEach(pair -> builders.add(visitPair(pair)));
//
//            if (builders.isEmpty()) {
//                return new MatchNoneQueryBuilder();
//            } else if (builders.size() == 1) {
//                return builders.get(0);
//            } else {
//                builders.forEach(query -> {
//                    if (ctx.op != null && ctx.op.getType() == KeyValuePairGrammarParser.AND) {
//                        res.must(query);
//                    } else {
//                        res.should(query);
//                    }
//                });
//                return res;
//            }
//        }

        @Override
        public AbstractQueryBuilder visitPair(KeyValuePairGrammarParser.PairContext ctx) {
            final String key = ctx.key().getText();
            final String value = ctx.value().getText().replace(',', ' ');

            return QueryBuilders.matchQuery(key, value);
        }
    }
}
