lexer grammar KeyValuePairLexer;

fragment ExtendedChar
    :   ExtendedStartChar
    |   '0'..'9'
    |   'A'..'Z'
    ;

fragment ExtendedStartChar
    :   'a'..'z'
    |   '\u0080'..'\u00FF'
    |   '\u0100'..'\u017F'
    |   '\u0180'..'\u024F'
    ;

AND : 'AND' ;
OR : 'OR' ;

U_CASE_WORD : [A-Z][A-Z0-9]* ;
L_CASE_WORD : ExtendedStartChar ExtendedChar* ;

COMBINATOR  : [,];
SEPARATOR   : [:];
LEFTBRACKET : [(];
RIGHTBRACKET: [)];

WHITESPACE  : [ \t\r\n]+ -> skip;
