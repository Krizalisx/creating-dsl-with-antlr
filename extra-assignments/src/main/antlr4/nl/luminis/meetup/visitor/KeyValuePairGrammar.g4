grammar KeyValuePairGrammar;
import KeyValuePairLexer;

query   : query AND query # And
        | query OR? query # Or
        | pair # queryPair
        | LEFTBRACKET query RIGHTBRACKET # parentheses
        ;
pair    : LEFTBRACKET key SEPARATOR value RIGHTBRACKET;
value   : L_CASE_WORD (COMBINATOR L_CASE_WORD)*;
key     : U_CASE_WORD ;