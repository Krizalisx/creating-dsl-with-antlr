// Generated from C:/Users/qpalz/Desktop/creating-dsl-with-antlr/assignment-2/src/main/antlr4/nl/luminis/meetup/kvp\KeyValuePairGrammar.g4 by ANTLR 4.8
package me.antlr.gen;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link KeyValuePairGrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface KeyValuePairGrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQuery(KeyValuePairGrammarParser.QueryContext ctx);
	/**
	 * Visit a parse tree produced by {@link KeyValuePairGrammarParser#pair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPair(KeyValuePairGrammarParser.PairContext ctx);
	/**
	 * Visit a parse tree produced by {@link KeyValuePairGrammarParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue(KeyValuePairGrammarParser.ValueContext ctx);
}