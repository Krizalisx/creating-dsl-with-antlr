// Generated from C:/Users/qpalz/Desktop/creating-dsl-with-antlr/assignment-1/src/main/antlr4/nl/luminis/meetup/helloworld\HelloGrammar.g4 by ANTLR 4.8
package me.antlr.gen;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

/**
 * This class provides an empty implementation of {@link HelloGrammarVisitor},
 * which can be extended to create a visitor which only needs to handle a subset
 * of the available methods.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public class HelloGrammarBaseVisitor<T> extends AbstractParseTreeVisitor<T> implements HelloGrammarVisitor<T> {
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitGreeting(HelloGrammarParser.GreetingContext ctx) { return visitChildren(ctx); }
}