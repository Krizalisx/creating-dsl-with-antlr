// Generated from C:/Users/qpalz/Desktop/creating-dsl-with-antlr/assignment-1/src/main/antlr4/nl/luminis/meetup/helloworld\HelloGrammar.g4 by ANTLR 4.8
package me.antlr.gen;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link HelloGrammarParser}.
 */
public interface HelloGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link HelloGrammarParser#greeting}.
	 * @param ctx the parse tree
	 */
	void enterGreeting(HelloGrammarParser.GreetingContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloGrammarParser#greeting}.
	 * @param ctx the parse tree
	 */
	void exitGreeting(HelloGrammarParser.GreetingContext ctx);
}