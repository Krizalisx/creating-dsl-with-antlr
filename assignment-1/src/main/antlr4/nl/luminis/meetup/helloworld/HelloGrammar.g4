grammar HelloGrammar;

greeting : 'Hello' NAME;

NAME : [a-zA-Z]+;
WS : [ \t\r\n]+ -> skip ; // skip whitespace
