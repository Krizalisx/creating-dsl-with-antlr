// Generated from C:/Users/qpalz/Desktop/creating-dsl-with-antlr/assignment-3/src/main/antlr4/nl/luminis/meetup/visitor\KeyValuePairGrammar.g4 by ANTLR 4.8
package me.antlr.gen;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class KeyValuePairGrammarLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		KEY=1, TERM=2, L_CASE_WORD=3, L_CASE_LETTER=4, DIGIT=5, COMBINATOR=6, 
		SEPARATOR=7, LEFTBRACKET=8, RIGHTBRACKET=9, WHITESPACE=10;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"KEY", "TERM", "L_CASE_WORD", "L_CASE_LETTER", "DIGIT", "COMBINATOR", 
			"SEPARATOR", "LEFTBRACKET", "RIGHTBRACKET", "WHITESPACE"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "KEY", "TERM", "L_CASE_WORD", "L_CASE_LETTER", "DIGIT", "COMBINATOR", 
			"SEPARATOR", "LEFTBRACKET", "RIGHTBRACKET", "WHITESPACE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public KeyValuePairGrammarLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "KeyValuePairGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\f;\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\3\2\3\2\7\2\32\n\2\f\2\16\2\35\13\2\3\3\3\3\5\3!\n\3\3\4\3\4\6\4%"+
		"\n\4\r\4\16\4&\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\6"+
		"\13\66\n\13\r\13\16\13\67\3\13\3\13\2\2\f\3\3\5\4\7\5\t\6\13\7\r\b\17"+
		"\t\21\n\23\13\25\f\3\2\13\3\2C\\\4\2\62;C\\\3\2c|\3\2\62;\3\2..\3\2<<"+
		"\3\2**\3\2++\4\2\13\f\17\17\2?\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t"+
		"\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2"+
		"\2\2\25\3\2\2\2\3\27\3\2\2\2\5\36\3\2\2\2\7$\3\2\2\2\t(\3\2\2\2\13*\3"+
		"\2\2\2\r,\3\2\2\2\17.\3\2\2\2\21\60\3\2\2\2\23\62\3\2\2\2\25\65\3\2\2"+
		"\2\27\33\t\2\2\2\30\32\t\3\2\2\31\30\3\2\2\2\32\35\3\2\2\2\33\31\3\2\2"+
		"\2\33\34\3\2\2\2\34\4\3\2\2\2\35\33\3\2\2\2\36 \5\t\5\2\37!\5\7\4\2 \37"+
		"\3\2\2\2 !\3\2\2\2!\6\3\2\2\2\"%\5\t\5\2#%\5\13\6\2$\"\3\2\2\2$#\3\2\2"+
		"\2%&\3\2\2\2&$\3\2\2\2&\'\3\2\2\2\'\b\3\2\2\2()\t\4\2\2)\n\3\2\2\2*+\t"+
		"\5\2\2+\f\3\2\2\2,-\t\6\2\2-\16\3\2\2\2./\t\7\2\2/\20\3\2\2\2\60\61\t"+
		"\b\2\2\61\22\3\2\2\2\62\63\t\t\2\2\63\24\3\2\2\2\64\66\t\n\2\2\65\64\3"+
		"\2\2\2\66\67\3\2\2\2\67\65\3\2\2\2\678\3\2\2\289\3\2\2\29:\b\13\2\2:\26"+
		"\3\2\2\2\b\2\33 $&\67\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}