// Generated from C:/Users/qpalz/Desktop/creating-dsl-with-antlr/assignment-3/src/main/antlr4/nl/luminis/meetup/visitor\KeyValuePairGrammar.g4 by ANTLR 4.8
package me.antlr.gen;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link KeyValuePairGrammarParser}.
 */
public interface KeyValuePairGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 */
	void enterQuery(KeyValuePairGrammarParser.QueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link KeyValuePairGrammarParser#query}.
	 * @param ctx the parse tree
	 */
	void exitQuery(KeyValuePairGrammarParser.QueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link KeyValuePairGrammarParser#pair}.
	 * @param ctx the parse tree
	 */
	void enterPair(KeyValuePairGrammarParser.PairContext ctx);
	/**
	 * Exit a parse tree produced by {@link KeyValuePairGrammarParser#pair}.
	 * @param ctx the parse tree
	 */
	void exitPair(KeyValuePairGrammarParser.PairContext ctx);
	/**
	 * Enter a parse tree produced by {@link KeyValuePairGrammarParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(KeyValuePairGrammarParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link KeyValuePairGrammarParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(KeyValuePairGrammarParser.ValueContext ctx);
}