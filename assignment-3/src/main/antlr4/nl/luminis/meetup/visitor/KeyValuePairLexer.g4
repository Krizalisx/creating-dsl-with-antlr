lexer grammar KeyValuePairLexer;

KEY         : [A-Z][A-Z0-9]* ;
TERM        : L_CASE_LETTER L_CASE_WORD?;

L_CASE_WORD : (L_CASE_LETTER | DIGIT)+;
L_CASE_LETTER : [a-z];
DIGIT : [0-9];

COMBINATOR  : [,];
SEPARATOR   : [:];
LEFTBRACKET : [(];
RIGHTBRACKET: [)];

WHITESPACE  : [\t\r\n]+ -> skip;
